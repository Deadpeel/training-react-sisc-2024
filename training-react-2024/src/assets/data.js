const notes = [
  {
    id: 1,
    body: "Agenda de azi:\n 1. Invat React\n 2. Cumpar flori pentru mama\n 3. Merg la sala",
    updated: "2024-01-20T23:15:01.000Z"
  },
  {
    id: 2,
    body: "Astazi voi invata React!",
    updated: "2024-02-18T23:15:01.000Z"
  },
  {
    id: 3,
    body: "Ma intalnesc cu Visan la o cafea la ora 14:00",
    updated: "2024-03-15T23:15:01.000Z"
  }
];

export default notes;
